# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Dev Variables
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# rds db password
resource "random_string" "password" {
  length  = 16
  special = false
}

variable "license_model" {
  description = "License model information for this DB instance. Optional, but required for some DB engines, i.e. Oracle SE1"
  type        = string
  default     = ""
}

variable "api_name" {
  description = "The database name"
  default     = "bcgapi"
}

variable "stage" {
  description = "The stage name"
  default     = "dev"
}

variable "user" {
  description = "The db user name"
  default     = "serverless"
}

variable "port" {
  description = "The db Port number"
  default     = 5432
}

# variable "password" {
#   description = "The db password"
#   default     =  random_string.password.result
# }
