# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Dev SSM PARAMETERS FOR POSTGRES DATABASE
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

resource "aws_ssm_parameter" "endpoint" {
  name = "/database/${var.api_name}/endpoint"
  description = "Endpoint to connect to the ${var.api_name}-${var.stage} database"
  type = "SecureString"
  value = module.postgres.this_db_instance_address
}

resource "aws_ssm_parameter" "user" {
  name = "/database/${var.api_name}/user"
  description = "Name of the ${var.api_name}-${var.stage} database user"
  type = "SecureString"
  value = var.user
}

resource "aws_ssm_parameter" "password" {
  name = "/database/${var.api_name}/password"
  description = "Password to the ${var.api_name}-${var.stage} database"
  type = "SecureString"
  value = random_string.password.result
}

resource "aws_ssm_parameter" "name" {
  name = "/database/${var.api_name}/name"
  description = "Name of the ${var.api_name}-${var.stage} database"
  type = "SecureString"
  value = "postgres"
}

resource "aws_ssm_parameter" "port" {
  name = "/database/${var.api_name}/port"
  description = "Port for the ${var.api_name}-${var.stage} database"
  type = "SecureString"
  value = var.port
}