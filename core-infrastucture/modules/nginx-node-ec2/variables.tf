
variable "aws_ami" {
  description = "Amazon Machine Images."
  # default = "ami-823686f5"
  default = "ami-050bc013"
}

variable "public_key_path" {
  description = "Key SSH public key to be used for authentication."
  type = string
}

variable "private_key_path" {
  description = "Path to the SSH private key to be used for authentication."
  type = string
}

variable "key_name" {
  default = "api-service-key"
}

variable "docker_compose_path" {
  description = "Docker compose file path"
  type = string
}

variable "nginx_config_path" {
  description = "Nginx compose file path"
  type = string
}

variable "app_source_path" {
  description = "App source file path"
  type = string
}