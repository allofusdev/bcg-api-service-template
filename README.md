# BCG Mobile Application Infrastructure Template With AWS

This is a skeleton template for rapidly developing full stack mobile applications.

![Architecural Diagram](./docs/images/sys.png)

# federated-graphql-server

The application runs federated apollo server on EC2 (guidance recommended by apollo to prevent timeouts on lambda coldstarts), and builds a service list from the `./resolver-service` folder. There are seperated services, deployed using the serverless framework.

One of the problems building and scaling graphql applications is schema bloat and general difficulties scaling a large graph. The benefits of graphql still out weigh the cons in a mobile environment, but this problem can become frustrating and prevent bottle necks. Apollo Federation attempts to solve this issue. The main downside of the federated approach, is we still need to approach the schema as one graph, that is compiled by the federated server, we can't run each service independantly.

# core-infrastructure

From out experience, we tend to find that seperating core infrastructure from core services offers many benefits. Here, we are using terraform to provision reusable core infrastructure. We tend to see the benefits of a remote data layer very quickly when building mobile applications, so here we've added a PostgresSQL database, partly to demonstrate the infrastructure pattern and partly as it posseses alot of strengths, given it's object relational nature over noSQL and traditional relational databases.

# rest-services

Decoupling business logic to small rest based services, and calling those services in graphql resolvers is a strong pattern as there are times when many parts of an application infrastructure all require the same operations from a universal source of truth. The case that this project is based on had core user data accesible from a mobile app, a dashboard, multiple push and SES messaging services and a crm integration, largely doing the same operations. Here it makes sense to decouple from the app entirely, and resolve this data when and where we need to.

        - A query in the dashboard service that returns a user by id using GET /user/{id}

            getUsersProfile(id: String) {
                id
                username
            }

        - A mutation that updates the user profile in the dashboard using  PATCH /user/{id}

            updateUserRecord(inputs: $inputs) {
                id
                username
            }

        - A query used around a RN app that returns the users data to them using  GET /user/{id}

            me {
                id
                username
            }

        - A legacy CRM integration to create and update new users POST /user & PATCH /user/{id}

Hence we see the performance benefit for apps using graphql, with the flexibility of decoupled microservices.
