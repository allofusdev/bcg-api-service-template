export type APIResponse = {
  statusCode: 200 | 400;
  body: string;
};
