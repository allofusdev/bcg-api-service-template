import createEvent from "aws-event-mocks";
import handler from "./handler";

const username = "test";

jest.mock("./api-utils", () => ({
  deleteUser: jest.fn(id => ({
    statusCode: 200,
    body: JSON.stringify({
      message: `Succesfully deleted user id ${id}`,
      data: {}
    })
  })),
  getUser: jest.fn(id => ({
    statusCode: 200,
    body: JSON.stringify({
      message: `Get user data for user id ${id}`,
      data: { id, username: "test" }
    })
  })),
  updateUser: jest.fn(id => ({
    statusCode: 200,
    body: JSON.stringify({
      message: `Get user data for user id ${id}`,
      data: { id, username: "amended test" }
    })
  })),
  createUser: jest.fn(() => ({
    statusCode: 200,
    body: JSON.stringify({
      message: `Get user data for user id id`,
      data: { id: "id", username: "test" }
    })
  }))
}));

describe("Api Methods", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("DELETE methof with no records.", async () => {
    const event = createEvent({
      template: "aws:apiGateway",
      merge: {
        body: null,
        pathParameters: { id: "id" },
        requestContext: { httpMethod: "DELETE" }
      }
    });

    const context = {
      callbackWaitsForEmptyEventLoop: false
    };

    const res = await handler(event, context);

    expect(res.statusCode).toBe(200);
  });

  it("GET method with no records.", async () => {
    const event = createEvent({
      template: "aws:apiGateway",
      merge: {
        body: null,
        pathParameters: { id: "id" },
        requestContext: { httpMethod: "GET" }
      }
    });

    const context = {
      callbackWaitsForEmptyEventLoop: false
    };

    const res = await handler(event, context);

    const { body } = res;

    const { data } = JSON.parse(body);

    expect(res.statusCode).toBe(200);
    expect(data).toEqual({ id: "id", username });
  });

  it("PUT method with no records.", async () => {
    const event = createEvent({
      template: "aws:apiGateway",
      merge: {
        body: JSON.stringify({ username: "amended test" }),
        pathParameters: { id: "id" },
        requestContext: { httpMethod: "PATCH" }
      }
    });

    const context = {
      callbackWaitsForEmptyEventLoop: false
    };

    const res = await handler(event, context);

    const { body } = res;

    const { data } = JSON.parse(body);

    expect(res.statusCode).toBe(200);
    expect(data).toEqual({ id: "id", username: "amended test" });
  });

  it("POST method with no records.", async () => {
    const event = createEvent({
      template: "aws:apiGateway",
      merge: {
        body: '{"username": "test"}',
        requestContext: { httpMethod: "POST" }
      }
    });

    const context = {
      callbackWaitsForEmptyEventLoop: false
    };

    const res = await handler(event, context);

    const { body } = res;

    const { data } = JSON.parse(body);

    expect(res.statusCode).toBe(200);
    expect(data).toEqual({ id: "id", username });
  });

  it("GET with no params method rejection.", async () => {
    const event = createEvent({
      template: "aws:apiGateway",
      merge: {
        requestContext: { httpMethod: "GET" },
        pathParameters: null
      }
    });

    const context = {
      callbackWaitsForEmptyEventLoop: false
    };

    const res = await handler(event, context);

    expect(res.statusCode).toBe(400);
  });
});
