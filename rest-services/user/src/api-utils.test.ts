import { deleteUser, getUser, updateUser, createUser } from "./api-utils";
import { Client } from "pg";

jest.mock("pg", () => {
  const mClient = {
    connect: jest.fn(),
    query: jest.fn(),
    end: jest.fn()
  };
  return { Client: jest.fn(() => mClient) };
});

describe("Api Methods -  deleteUser, getUser, updateUser, createUser", () => {
  let client;

  beforeEach(() => {
    client = new Client();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("DeleteUser query with no records.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });

    const res = await deleteUser("id");

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("DeleteUser query with error.", async () => {
    client.query.mockRejectedValueOnce({ error: "error" });

    const res = await deleteUser("id");

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("DeleteUser query with no records.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 1 });

    const res = await deleteUser("id");

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(200);
  });

  it("getUser query with no records.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });

    const res = await getUser("id");

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("getUser query with error.", async () => {
    client.query.mockRejectedValueOnce({ error: "error" });

    const res = await getUser("id");

    expect(client.connect).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("getUser query with user records.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 1 });

    const res = await getUser("id");

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(200);
  });

  it("updateUser query with user records.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 1 });

    const res = await updateUser("id", {
      username: "testing user"
    });

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(200);
  });

  it("updateUser query with no user records.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 0 });

    const res = await updateUser("id", {
      username: "testing user"
    });

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("updateUser query with empty body.", async () => {
    client.query.mockResolvedValueOnce({ rows: [], rowCount: 1 });

    const res = await updateUser("id", {});

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(200);
  });

  it("updateUser query with error.", async () => {
    client.query.mockRejectedValueOnce({ error: "error" });

    const res = await updateUser("id", {});

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("Create a user, mocking the api response.", async () => {
    client.query
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockImplementationOnce(() => ({
        rows: [
          {
            id: "ed969a72-7218-4999-b47e-4b4d2c208d16",
            username: "testing user",
            created_on: "2020-08-12T08:50:39.812Z"
          }
        ],
        rowCount: 1
      }));

    const res = await createUser({ username: "testing user" });

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(200);

    const { body } = res;

    const { data } = JSON.parse(body);

    expect(data).toEqual({
      id: "ed969a72-7218-4999-b47e-4b4d2c208d16",
      username: "testing user",
      created_on: "2020-08-12T08:50:39.812Z"
    });
  });

  it("Create a user, mocking the null api response from create user postgres client.", async () => {
    client.query
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockImplementationOnce(() => null)
      .mockImplementationOnce(() => ({
        rows: [
          {
            id: "ed969a72-7218-4999-b47e-4b4d2c208d16",
            username: "testing user",
            created_on: "2020-08-12T08:50:39.812Z"
          }
        ],
        rowCount: 1
      }));

    const res = await createUser({ username: "testing user" });

    expect(client.connect).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("Create a user, mocking the null api response from create user postgres client.", async () => {
    client.query
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockImplementationOnce(() => null);

    const res = await createUser({ username: "testing user" });

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("Create a user, mocking a failed promise rejection on the first client call.", async () => {
    client.query.mockRejectedValueOnce({ error: "error" });

    const res = await createUser({ username: "testing user" });

    expect(client.connect).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("Create a user, mocking a failed promise rejection on the second client call.", async () => {
    client.query
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockRejectedValueOnce({ error: "error" });

    const res = await createUser({ username: "testing user" });

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });

  it("Create a user, mocking a failed promise rejection on the third client call.", async () => {
    client.query
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockImplementationOnce(() => ({
        command: "CREATE",
        rowCount: null
      }))
      .mockRejectedValueOnce({ error: "error" });

    const res = await createUser({ username: "testing user" });

    expect(client.connect).toBeCalledTimes(1);
    expect(client.end).toBeCalledTimes(1);
    expect(res.statusCode).toBe(400);
  });
});
