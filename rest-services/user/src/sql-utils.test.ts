import { insert, deleteById, getById, updateById } from "./sql-utils";

describe("sql-utils methods", () => {
  it("inserts a record based on table name and body", async () => {
    const table = "user_table";

    const input = {
      username: "Hello World"
    };

    expect(insert(table, input)).toBe(
      `INSERT INTO user_table (username) VALUES ('Hello World') RETURNING *`
    );
  });

  it("deletes a user based on id", async () => {
    const table = "user_table";

    const id = "id";

    expect(deleteById(table, id)).toBe(`DELETE FROM user_table WHERE id='id'`);
  });

  it("gets a user based on id", async () => {
    const table = "user_table";

    const id = "id";

    expect(getById(table, id)).toBe(`SELECT * FROM user_table WHERE id='id'`);
  });

  it("updates a user based on id", async () => {
    const table = "user_table";

    const id = "id";

    const input = {
      username: "Hello World"
    };

    console.log(updateById(table, id, input), "updateById(table, id, input)");

    expect(updateById(table, id, input)).toBe(
      `UPDATE user_table SET username='Hello World' WHERE id='id'`
    );
  });

  it("updates a user based on id with a number", async () => {
    const table = "user_table";

    const id = "id";

    const input = {
      username: "Hello World",
      number: 1
    };

    expect(updateById(table, id, input)).toBe(
      `UPDATE user_table SET username='Hello World',number=1 WHERE id='id'`
    );
  });
});
