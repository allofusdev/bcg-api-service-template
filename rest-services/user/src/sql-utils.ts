/**
 * Simple PostgresSql utils for creating, updating, getting and deleting user records.
 */

/**
 * Insert values to table based on inputs
 *
 * @function insert
 * @return {String} Returns a PostgresSQL query string
 */

export const insert = (
  tableName: string,
  obj: { [key: string]: string }
): string => {
  const keys = Object.keys(obj);

  const values = keys.map(key => obj[key]);
  const cols = keys.join(", ");
  const params = values.map(index => `'${index}'`).join(", ");

  const sql = `INSERT INTO ${tableName} (${cols}) VALUES (${params}) RETURNING *`;

  console.log("insert: sql =", sql);

  return sql;
};

/**
 * Deletes user record by id
 *
 * @function deleteById
 * @return {String} Returns a PostgresSQL query string
 */

export const deleteById = (tableName: string, id: string): string => {
  const sql = `DELETE FROM ${tableName} WHERE id='${id}'`;

  console.log("deleteById: sql =", sql);

  return sql;
};

/**
 * Gets user record by id
 *
 * @function getById
 * @return {String} Returns a PostgresSQL query string
 */

export const getById = (tableName: string, id: string): string => {
  const sql = `SELECT * FROM ${tableName} WHERE id='${id}'`;
  console.log("getById: sql =", sql);

  return sql;
};

/**
 * Updates user record by id
 *
 * @function updateById
 * @return {String} Returns a PostgresSQL query string
 */

export const updateById = (
  tableName: string,
  id: string,
  obj: { [key: string]: string | number }
) => {
  const sets = Object.keys(obj).map(key => {
    const v = obj[key];
    const value = typeof v === "string" ? `'${v}'` : v;
    return `${key}=${value}`;
  });

  const sql = `UPDATE ${tableName} SET ${sets} WHERE id='${id}'`;

  console.log("update: sql =", sql);
  return sql;
};
