/**
 * Describes a simple api to create, update, delete and get a user record from
 * the user_list table in the API database.
 */

import { Client } from "pg";
import { insert, deleteById, getById, updateById } from "./sql-utils";
import { APIResponse } from "./types";

const USER_LIST_TABLE: string = "user_list";

/**
 * Deletes a user from the user_list table
 *
 * @async
 * @function deleteUser
 * @return {Promise} Returns a aws api gateway response
 */

export async function deleteUser(id: string): Promise<APIResponse> {
  const client: Client = new Client();

  const deleteUserQuery: string = deleteById(USER_LIST_TABLE, id);

  try {
    await client.connect();

    const deleteUserQueryResponse = await client.query(deleteUserQuery);

    if (deleteUserQueryResponse && deleteUserQueryResponse.rowCount !== 0) {
      return {
        statusCode: 200,
        body: JSON.stringify({
          message: `Succesfully deleted user id ${id}`,
          data: deleteUserQueryResponse
        })
      };
    }

    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There is no user with the id ${id}`
      })
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There was a problem adding user`,
        error
      })
    };
  } finally {
    await client.end();
  }
}

/**
 * Gets a user from the user_list table
 *
 * @async
 * @function getUser
 * @return {Promise} Returns a aws api gateway response
 */

export async function getUser(id: string): Promise<APIResponse> {
  const client: Client = new Client();
  const getUserQuery: string = getById(USER_LIST_TABLE, id);

  try {
    await client.connect();

    const getUserQueryResponse = await client.query(getUserQuery);

    if (getUserQueryResponse && getUserQueryResponse.rowCount !== 0) {
      return {
        statusCode: 200,
        body: JSON.stringify({
          message: `Get user data for user id ${id}`,
          data: getUserQueryResponse.rows[0]
        })
      };
    }

    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There is no user with the id ${id}`
      })
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There was a problem adding user`,
        error
      })
    };
  } finally {
    await client.end();
  }
}

/**
 * Updates a user in the user_list table
 *
 * @async
 * @function updateUser
 * @return {Promise} Returns a aws api gateway response
 */

export async function updateUser(id: string, body: any): Promise<APIResponse> {
  const client: Client = new Client();
  const updateUserQuery: string = updateById(USER_LIST_TABLE, id, body);

  try {
    await client.connect();

    const updateUserQueryResponse = await client.query(updateUserQuery);

    if (updateUserQueryResponse && updateUserQueryResponse.rowCount !== 0) {
      return {
        statusCode: 200,
        body: JSON.stringify({
          message: `Updated user data for user id ${id}`,
          data: body
        })
      };
    }

    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There is no user with the id ${id}`
      })
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There was a problem adding user`,
        error
      })
    };
  } finally {
    await client.end();
  }
}

/**
 * Creates a user in the user_list table
 *
 * @async
 * @function createUser
 * @return {Promise} Returns a aws api gateway response
 */

export async function createUser(body: any): Promise<APIResponse> {
  const client: Client = new Client();

  const createTableQuery: string = `
      CREATE TABLE IF NOT EXISTS ${USER_LIST_TABLE} (
          id UUID NOT NULL DEFAULT uuid_generate_v4(),
          username VARCHAR ( 50 ) UNIQUE NOT NULL,
          created_on TIMESTAMP NOT NULL
      );
  `;

  const createUserQuery: string = insert("user_list", {
    created_on: new Date().toISOString(),
    ...body
  });

  try {
    await client.connect();

    await client.query(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`);

    const userTableQueryResponse = await client.query(createTableQuery);

    const createUserQueryResponse = await client.query(createUserQuery);

    if (userTableQueryResponse && createUserQueryResponse) {
      return {
        statusCode: 200,
        body: JSON.stringify({
          message: `Succesfully added user`,
          data: createUserQueryResponse.rows[0]
        })
      };
    }

    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There was a problem adding user`
      })
    };
  } catch (error) {
    console.log(`Returns error from pool ${error}`);

    return {
      statusCode: 400,
      body: JSON.stringify({
        message: `There was a problem adding user`,
        error
      })
    };
  } finally {
    await client.end();
  }
}
