import { createUser, deleteUser, getUser, updateUser } from "./api-utils";
import { APIGatewayEvent } from "aws-lambda";

import { APIResponse } from "./types";

/**
 * UserService Lambda that returns user based properties from PostgresSql database.
 *
 * @async
 * @function handler
 * @return {Promise} Returns a aws api gateway response
 */

export default async (
  event: APIGatewayEvent,
  context: any
): Promise<APIResponse> => {
  const { pathParameters } = event;
  const { httpMethod } = event.requestContext;

  const body = event.body && JSON.parse(event.body);

  if (httpMethod !== "POST" && !pathParameters) {
    return {
      statusCode: 400,
      body: "Requires an id path paramater."
    };
  }

  let userId;

  if (pathParameters && pathParameters.id) {
    userId = pathParameters.id;
  }

  context.callbackWaitsForEmptyEventLoop = false;

  try {
    switch (httpMethod) {
      case "POST":
        return await createUser(body);
      case "PATCH":
        return await updateUser(userId, body);
      case "GET":
        return await getUser(userId);
      case "DELETE":
        return await deleteUser(userId);
    }
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        error
      })
    };
  }
};
