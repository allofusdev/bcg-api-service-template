# Generic User Service

An example of a generic user rest based micro service that we might find in our app eco system.

Here we can create, update delete and get data associated with a user. Hence it can be integrated with any part of a business
ecosystem for example an app, a dashboard or a crm integration. This example of decoupling reduces dependancies on other services.

        POST /user
        PATCH /user/{id}
        GET /user/{id}
        DELETE /user/{id}

The service references infrastructure defined in our `core-infrastructure` terraform templates via ssm, but we can reference and create our own tables independantly.

    For example...


        CREATE TABLE IF NOT EXISTS user_list (
            id serial PRIMARY KEY,
            username VARCHAR ( 50 ) UNIQUE NOT NULL,
            created_on TIMESTAMP NOT NULL
        );

## Running locally

We can use the serverless offline plug to run our api gateway locally. As long as we're logged into the aws cli with a valid
user, we can read and write to our PostgresSql database and access the ssm parameters.

        $ npm install
        $ serverless offline

[Here](https://www.getpostman.com/collections/83b6b87a02c1180696c0) is the POSTMAN workspace for the api, here we can test the api methods.

# CI / CD

    CI / CD info here...

# Versioning

Versioning info here ...

# Documentation

Documentation info here ...

# Testing

Testing info here...
