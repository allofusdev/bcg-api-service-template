import React from 'react';
import {Box} from '.';
import {BoxProps} from './Box';

type AbsoluteBoxProps = BoxProps & {
  top?: number | string;
  right?: number | string;
  bottom?: number | string;
  left?: number | string;
  children?: React.ReactNode;
};

export default function AbsoluteBox({
  top,
  right,
  bottom,
  left,
  ...props
}: AbsoluteBoxProps) {
  return (
    <Box
      {...props}
      style={[{position: 'absolute', top, right, bottom, left}, props.style]}
    />
  );
}
