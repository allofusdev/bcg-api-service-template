import React from 'react';
import {storiesOf} from '@storybook/react-native';
import tinycolor from 'tinycolor2';

import {colors, fontSizes, spaces} from './theme';
import {Box, Text} from '.';
import {FontSizeType} from './Text';
import {ColorProps, SpaceScale} from './Base';

storiesOf('Primitives/Text', module)
  .addDecorator((story: () => React.ReactNode) => {
    return (
      <Box bg="white" p={2} style={{flex: 1}}>
        {story()}
      </Box>
    );
  })
  .add('Different sizes', () => (
    <Box>
      {Object.keys(fontSizes)
        .reverse()
        .map(fs => (
          <Text key={fs} fs={(fs as unknown) as FontSizeType} mb={2}>
            {fs} – Type something
          </Text>
        ))}
    </Box>
  ))
  .add('Different colors', () => {
    const listOfColors = Object.keys(colors);
    const brightColors = listOfColors.filter(key =>
      tinycolor.isReadable(colors[key as ColorProps], '#333'),
    );
    const darkColors = listOfColors.filter(
      key => !tinycolor.isReadable(colors[key as ColorProps], '#333'),
    );
    return (
      <Box>
        {darkColors.map(color => (
          <Box key={color} p={1} bg="white">
            <Text color={color as ColorProps}>{color}</Text>
          </Box>
        ))}
        {brightColors.map(color => (
          <Box key={color} p={1} bg="black">
            <Text color={color as ColorProps}>{color}</Text>
          </Box>
        ))}
      </Box>
    );
  })
  .add('Different styles', () => (
    <Box>
      <Text bold fs={0} mb={2}>
        The quick brown fox jumps over the lazy dog
      </Text>
      <Text italic fs={0} mb={2}>
        The quick brown fox jumps over the lazy dog
      </Text>
      <Text caps fs={0} mb={2}>
        The quick brown fox jumps over the lazy dog
      </Text>
      <Text center fs={0} mb={2}>
        The quick brown fox jumps over the lazy dog
      </Text>
    </Box>
  ))
  .add('Different margins', () => (
    <Box>
      {Object.keys(spaces).map(space => (
        <Text key={space} mb={space as SpaceScale}>
          <Text bold>{space}</Text> The quick brown fox jumps over the lazy dog
        </Text>
      ))}
      <Text fs={0}>end</Text>
    </Box>
  ));
