import {Dimensions} from 'react-native';
import {colors} from './theme';
import {ColorProps} from '.';

export function getColor(color: string | undefined) {
  if (!color) return;
  return color in colors ? colors[color as ColorProps] : color;
}

export const isSmallPhone = Dimensions.get('screen').width === 320;
