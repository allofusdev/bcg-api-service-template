import {isSmallPhone} from './utils';

export const fontSizes = isSmallPhone
  ? {
      0: 14,
      1: 20,
      2: 28,
      3: 50,
    }
  : {
      0: 16,
      1: 22,
      2: 32,
      3: 60,
    };
export const letterSpacings = isSmallPhone
  ? {
      0: -0.56,
      1: -0.8,
      2: -1.12,
      3: -1.6,
    }
  : {
      0: -0.64,
      1: -0.88,
      2: -1.28,
      3: -2.4,
    };
export const lineHeights = isSmallPhone
  ? {
      0: 22,
      1: 26,
      2: 32,
      3: 54,
    }
  : {
      0: 24,
      1: 28,
      2: 36,
      3: 64,
    };

// Limit how much fonts can grow by, if the user is using larger/accessible fonts
export const maxScaledSizes = {
  0: 1.6,
  1: 1.2,
  2: 1,
  3: 1,
};

export const spacesInt = isSmallPhone
  ? [0, 6, 12, 24, 36, 52]
  : [0, 10, 20, 40, 60, 80];

export const spaces = spacesInt.map(space => `${space}px`);

export const borderRadius = [0, '30px', '999px'];

export const colors = {
  accent: '#ffc436',
  text: '#ffc436',
  white: '#ffffff',
  black: '#292827',
};
