import styled, {css} from 'styled-components/native';
import {borderRadius} from './theme';
import {
  SpaceProps,
  baseStyle,
  ColorProps,
  DisplayProps,
  displayStyle,
} from './Base';
import {ViewProps} from 'react-native';
import {getColor} from './utils';

export type BorderRadiusProps = 0 | 1 | 2;

export type BoxProps = ViewProps &
  SpaceProps &
  DisplayProps & {
    bg?: ColorProps | string;
    opacity?: number;
    br?: BorderRadiusProps;
  };

const Box = styled.View<BoxProps>`
  ${baseStyle};
  ${displayStyle};

  ${props =>
    props.bg &&
    css`
      background-color: ${getColor(props.bg)};
    `}

  ${props =>
    props.br &&
    css`
      border-radius: ${borderRadius[props.br]};
    `}

  ${props =>
    props.opacity &&
    css`
      opacity: ${props.opacity};
    `}
`;

export default Box;
