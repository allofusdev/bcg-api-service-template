import styled, {css} from 'styled-components/native';
import {TextStyle} from 'react-native';

import {
  colors,
  fontSizes,
  lineHeights,
  letterSpacings,
  maxScaledSizes,
} from './theme';
import {SpaceProps, baseStyle, ColorProps} from './Base';

export type FontSizeType = 0 | 1 | 2 | 3;

type DisplayProps = {
  color?: ColorProps;
  opacity?: number;
  fs?: FontSizeType;
  lh?: FontSizeType;
  center?: boolean;
  underline?: boolean;
  bold?: boolean;
  italic?: boolean;
  caps?: boolean;
};

export type TextProps = SpaceProps & DisplayProps;

/**
 * There are inconsitencies with the font family name on iOS and Android
 * So we have to render different fonts depending on the platform
 */
export const getFontFamily = (props: DisplayProps): TextStyle => {
  return {
    fontWeight: props.bold ? '700' : '400',
    fontStyle: props.italic ? 'italic' : 'normal',
  };
};

const BaseText = styled.Text.attrs((props: TextProps) => ({
  maxFontSizeMultiplier: maxScaledSizes[props.fs as FontSizeType] || 1.5,
}))<TextProps>`
  ${baseStyle};
  font-size: ${props => fontSizes[props.fs || 0]}px;
  line-height: ${props => lineHeights[props.fs || 0]}px;
  letter-spacing: ${props => letterSpacings[props.fs || 0]}px;
  color: ${props => colors[props.color || 'text']};
  opacity: ${props => props.opacity || 1};
  text-decoration: ${props => (props.underline ? 'underline' : 'none')};
  ${props =>
    props.lh &&
    css`
      line-height: ${props.lh};
    `}
  ${props => props.center && `text-align: center;`}
  ${props => props.caps && 'text-transform: uppercase;'}
`;

// @ts-ignore we have to mix css template literal with object and TypeScript is complaining about it
const Text = styled(BaseText)(getFontFamily);

Text.defaultProps = {
  color: 'text',
  fs: 0,
  bold: false,
  italic: false,
  opacity: 1,
  lh: undefined,
};

export default Text;
