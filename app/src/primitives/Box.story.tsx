import React from 'react';
import {ViewStyle, ScrollView} from 'react-native';
import {storiesOf} from '@storybook/react-native';

import {Box, Text} from '.';
import {borderRadius, colors, spaces} from './theme';
import {ColorProps, SpaceScale} from './Base';
import {BorderRadiusProps} from './Box';

const boxProps = {
  style: {
    shadowOpacity: 0.1,
    shadowRadius: 3,
    shadowOffset: {height: 1},
    elevation: 3,
  } as ViewStyle,
};

storiesOf('Primitives/Box', module)
  .addDecorator((story: () => React.ReactNode) => (
    <ScrollView style={{flex: 1}}>{story()}</ScrollView>
  ))
  .add('Different background colors', () => (
    <Box p={2}>
      {Object.keys(colors).map(color => (
        <React.Fragment key={color}>
          <Box
            bg={color as ColorProps}
            br={1}
            w="100%"
            h={60}
            mb={1}
            {...boxProps}
          />
          <Text mb={2}>{color}</Text>
        </React.Fragment>
      ))}
    </Box>
  ))
  .add('Different border radius', () => (
    <Box p={2}>
      {Object.keys(borderRadius).map(br => (
        <React.Fragment key={br}>
          <Box
            bg="accent"
            br={(br as unknown) as BorderRadiusProps}
            w="100%"
            h={60}
            mb={1}
            {...boxProps}
          />
          <Text mb={2}>{br}</Text>
        </React.Fragment>
      ))}
    </Box>
  ))
  .add('Different margins', () => (
    <Box p={2}>
      {Object.keys(spaces).map(space => (
        <Box
          key={space}
          bg="accent"
          br={1}
          w="100%"
          h={60}
          p={1}
          mb={space as SpaceScale}
          {...boxProps}>
          <Text>mb: {space}</Text>
        </Box>
      ))}

      <Box bg="accent" w="100%" h={2} {...boxProps} />
    </Box>
  ));
