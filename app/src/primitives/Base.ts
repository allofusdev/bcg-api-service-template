import {css} from 'styled-components/native';
import {spaces, colors} from './theme';

export type SpaceScale = 0 | 1 | 2 | 3 | 4 | 5 | 'auto';

export type SpaceProps = {
  m?: SpaceScale;
  mv?: SpaceScale;
  mh?: SpaceScale;
  mb?: SpaceScale;
  mt?: SpaceScale;
  ml?: SpaceScale;
  mr?: SpaceScale;

  p?: SpaceScale;
  pv?: SpaceScale;
  ph?: SpaceScale;
  pb?: SpaceScale;
  pt?: SpaceScale;
  pl?: SpaceScale;
  pr?: SpaceScale;
};

export type DisplayProps = {
  row?: boolean;
  w?: number | string;
  h?: number | string;
  f?: number | string;
  justify?:
    | 'flex-start'
    | 'center'
    | 'flex-end'
    | 'space-between'
    | 'space-around';
  flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  align?: 'flex-start' | 'center' | 'flex-end';
  z?: number;
};

export type ColorProps = keyof typeof colors;

const getSpace = (property: string, key: keyof SpaceProps) => (
  props: SpaceProps,
) => {
  const value = props[key];
  if (value === 'auto') return `${property}: auto;`;
  if (value !== undefined) return `${property}: ${spaces[value]};`;
  return undefined;
};

export const baseStyle = css<SpaceProps>`
  ${getSpace('margin', 'm')};
  ${getSpace('margin-vertical', 'mv')};
  ${getSpace('margin-horizontal', 'mh')};
  ${getSpace('margin-top', 'mt')};
  ${getSpace('margin-bottom', 'mb')};
  ${getSpace('margin-left', 'ml')};
  ${getSpace('margin-right', 'mr')};

  ${getSpace('padding-vertical', 'pv')};
  ${getSpace('padding-horizontal', 'ph')};
  ${getSpace('padding-top', 'pt')};
  ${getSpace('padding-bottom', 'pb')};
  ${getSpace('padding-left', 'pl')};
  ${getSpace('padding-right', 'pr')};
  ${getSpace('padding', 'p')};
`;

export const displayStyle = css<DisplayProps>`
  ${props => props.row && 'flex-direction: row;'}

  ${props => props.w && `width: ${props.w};`}

  ${props => props.h && `height: ${props.h};`}

  ${props => props.f && `flex: ${props.f};`}

  ${props => props.justify && `justify-content: ${props.justify};`}

  ${props => props.flexDirection && `flex-direction: ${props.flexDirection};`}

  ${props => props.align && `align-items: ${props.align};`}

  ${props => props.z && `z-index: ${props.z};`}
`;
