import React, {ReactNode} from 'react';
import {ApolloProvider} from '@apollo/client';
import useApolloCache from './hooks/useApolloCache';

export default function Providers({children}: {children: ReactNode}) {
  const {client} = useApolloCache();

  if (client) {
    return <ApolloProvider client={client}>{children}</ApolloProvider>;
  }

  return null;
}
