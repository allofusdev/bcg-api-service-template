import React, {ReactNode} from 'react';
import Providers from './Providers';
import {useQuery, gql} from '@apollo/client';
import {Box, Text} from './primitives';

const QUERY = gql`
  query Me($id: ID!) {
    me(id: $id) {
      id
      username
    }
  }
`;

function Me() {
  const {loading, error, data} = useQuery(QUERY, {
    variables: {id: '2d46cfce-7930-49b5-bc94-429ac4c54ccc'},
  });

  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text>Error :(</Text>;

  const {id, username} = data.me;

  return (
    <Box f={1} bg="black">
      <Box pt={5} pl={2} pr={2}>
        <Text fs={2} pb={1} bold>
          username: {username}
        </Text>

        <Text fs={1}>id: {id}</Text>
      </Box>
    </Box>
  );
}

const App: () => ReactNode = () => {
  return (
    <Providers>
      <Me />
    </Providers>
  );
};

export default App;
