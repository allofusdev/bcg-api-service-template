import {useState, useEffect, Dispatch, SetStateAction} from 'react';
import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client';
import AsyncStorage from '@react-native-community/async-storage';
import {persistCache} from 'apollo-cache-persist';

const GRAPHQL_URL = 'http://ec2-3-85-34-231.compute-1.amazonaws.com/graphql';

type TClient = ApolloClient<NormalizedCacheObject> | null;
type TSetClient = Dispatch<SetStateAction<TClient | null>>;

export default function(): {
  client: TClient;
  setClient: TSetClient;
} {
  const [client, setClient] = useState<TClient>(null);

  useEffect(() => {
    const cache = new InMemoryCache();

    persistCache({
      cache,
      storage: AsyncStorage as any,
    }).then(() => {
      setClient(
        new ApolloClient({
          uri: GRAPHQL_URL,
          cache: cache,
        }),
      );
    });
  }, []);

  return {client, setClient};
}
