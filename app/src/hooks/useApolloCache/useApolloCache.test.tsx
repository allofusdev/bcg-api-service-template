import {renderHook, act} from '@testing-library/react-hooks';
import useApolloCache from './useApolloCache';

test('Should render null unless setClient is set to an apollo client', async () => {
  const {result} = renderHook(() => useApolloCache());

  expect(result.current.client).toBe(null);

  const mockClient = jest.fn(prevState => ({...prevState, apolloClient: {}}));

  const mockResponse = mockClient(null);

  act(() => {
    result.current.setClient(mockResponse);
  });

  expect(result.current.client).toEqual(mockResponse);
});
