import Reactotron, {
  trackGlobalErrors,
  storybook,
} from 'reactotron-react-native';
import {NativeModules} from 'react-native';

// grabs the ip address
const host = NativeModules.SourceCode.scriptURL.split('://')[1].split(':')[0];

Reactotron.configure({host})
  .useReactNative({
    storybook: true,
    networking: {
      ignoreUrls: /symbolicate/,
    },
  })
  .use(storybook())
  .use(trackGlobalErrors())
  .connect();

console.tron = Reactotron;
