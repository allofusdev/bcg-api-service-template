// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../src/primitives/Box.story');
  require('../src/primitives/Text.story');
}

const stories = ['../src/primitives/Box.story', '../src/primitives/Text.story'];

module.exports = {
  loadStories,
  stories,
};
