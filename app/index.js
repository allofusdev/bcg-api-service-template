/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

const SafeApp = () => {
  const App = require('./src/App').default;
  return <App />;
};

if (__DEV__) {
  require('./src/ReactotronConfig');

  // The inclusion of Storybook inside a React Native app seems to remove the implementation of Promise.prototype.finally.
  // Causing the Promise.resolve(...).finally is not a function error to occur.
  // Adding temp fix https://github.com/storybookjs/storybook/issues/8371

  const fn = Promise.prototype.finally;

  const {StorybookUI} = require('./storybook');

  Promise.prototype.finally = fn;

  const AppWrapper = console.tron.storybookSwitcher(StorybookUI)(SafeApp);

  AppRegistry.registerComponent(appName, () => AppWrapper);
} else {
  AppRegistry.registerComponent(appName, () => SafeApp);
}
