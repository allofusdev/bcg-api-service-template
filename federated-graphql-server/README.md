# Federated graphql service

Often a simple apollo server is sufficient for an api's need, but more often than not on a growing production app, a single instance can become difficult to scale and manage. Apollo federated servers offer a neat solution to this problem.

There is a good run down of apollo federation [here](https://www.apollographql.com/docs/apollo-server/federation/introduction/). Generally the principle is to break down large graphs to smaller services, whilst allowing each service to push dependancies on each other.

    Apollo Federation uses a declarative programming model that enables each implementing service to implement only the part of your data graph that it should be responsible for. This way, your organization can represent an enterprise-scale data graph as a collection of separately maintained GraphQL services.

This service runs a federated apollo server, with a simple user service, to demonstrate this pattern.

## Running locally

1. When adding a new service, add a new command to start the service, including it's unique port. Serverless will run these offline.

```
 "start-service-user": "cd ./resolver-services/User && sls offline start --port 4001"
```

2. run the start services command to spin up these services.

```
    $ npm run start-services
```

3. Run the apollo federation server at http://localhost:8081/graphql

```
    $ npm run start-gateway
```

4. Opening port 8081 will open a graphIQL instance and try out a query.

```
    query($id: ID!) {
        me(id: $id)
            id
        }
    }
```

## Generating a service list

The federated server runs on an ec2 instance as it needs to compile schemas on startup, hence a lambdas function would need to do this on every cold start. The resolver services are simple api gateway services running with a lambda function. We need to generate our service list for the federated server when we deploy the app.

1. To generate a service list before push our ec2 instance, run

```
    $ REGION=eu-west-1 STAGE=dev node ./generate-service-list.js
```

2. Then in our root folder, run terraform apply to deploy a ec2 instance running our services.

```
    $ terraform apply
```
