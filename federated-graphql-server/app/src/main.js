const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const { ApolloGateway } = require("@apollo/gateway");
const generatedServiceList = require("./services.json");

const localConfig = [
  {
    name: "UserService",
    url: "http://localhost:4001/graphql"
  }
];

const { ENVIRONMENT } = process.env;

const serviceList =
  ENVIRONMENT === "local" ? localConfig : generatedServiceList;

const PORT = process.env.WWW_PORT || 8081;

const app = express();

// Initialize an ApolloGateway instance and pass it an array of
// your implementing service names and URLs
const gateway = new ApolloGateway({
  serviceList
});

// Pass the ApolloGateway to the ApolloServer constructor
const server = new ApolloServer({
  gateway,

  // Disable subscriptions (not currently supported with ApolloGateway)
  subscriptions: false
});

server.applyMiddleware({ app });

app.listen({ port: PORT }, () =>
  console.log(`🚀 Server ready at http://localhost:8081${server.graphqlPath}`)
);
