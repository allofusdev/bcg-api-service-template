const util = require("util");
const { exec } = require("child_process");
const { readdirSync } = require("fs");
const fs = require("fs");

const execProm = util.promisify(exec);

const STAGE = process.env.STAGE;
const REGION = process.env.REGION;

/**
 * Runs a shell script with exec and returns it's values.
 *
 * @async
 * @function runShell
 * @return {Promise} Returns an object stdout and sterror outputs
 */

async function runShell(command) {
  let result;
  try {
    result = await execProm(command);
  } catch (ex) {
    result = ex;
  }
  if (Error[Symbol.hasInstance](result)) return;

  return result;
}

/**
 * Returns a service list for the apollo federated server
 *
 * @async
 * @function returnResolverServices
 * @return {Promise} Returns a list with the name and url of each service.
 */

const returnResolverServices = () => {
  const directory = "resolver-services";

  /* Loop through services and get info to construct the servistList array for the federated server */
  return Promise.all(
    readdirSync(directory, { withFileTypes: true })
      .filter(dirent => dirent.isDirectory())
      .map(async dirent => {
        /* Each service must be deployed so that we can get their service info. */
        await runShell(
          `cd ./${directory}/${dirent.name} && sls deploy --stage ${STAGE}`
        );

        /* Return the service name from serverless dir */
        const { stdout: serviceNameResponse } = await runShell(
          `cd ./${directory}/${dirent.name} && sls output get --name serviceName`
        );

        const name = serviceNameResponse.replace(/\n/g, "");

        const stackName = `${name}-${STAGE}`;

        /* Return the rest api id from cloudformation stack */
        const { stdout: stackData } = await runShell(
          `aws --region ${REGION} cloudformation describe-stack-resources --stack-name ${stackName}`
        );

        const restApiId = JSON.parse(stackData).StackResources.filter(
          index => index.ResourceType === "AWS::ApiGateway::RestApi"
        )[0].PhysicalResourceId;

        /* Construct the rest api graphql endpoint*/
        const url = `https://${restApiId}.execute-api.${REGION}.amazonaws.com/${STAGE}/graphql`;

        return {
          name,
          url
        };
      })
  );
};

/* Writes the service list to JSON */
returnResolverServices().then(res => {
  fs.writeFile("./app/src/services.json", JSON.stringify(res), async err => {
    if (err) throw err;

    console.log("Successfully generated the service list file");
  });
});
