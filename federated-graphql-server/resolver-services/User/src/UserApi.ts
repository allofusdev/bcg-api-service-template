import { RESTDataSource } from "apollo-datasource-rest";
import { User } from "./types";

export default class UserAPI extends RESTDataSource {
  constructor() {
    super();

    this.baseURL =
      "https://n86npnn6qh.execute-api.eu-west-1.amazonaws.com/dev/";
  }

  willSendRequest(request: any) {
    const headers = {
      "x-api-key": this.context.token,
      "Content-Type": "application/json"
    };

    for (const [key, value] of Object.entries(headers)) {
      request.headers.set(key, value);
    }
  }

  async getUser(id: string): Promise<{ data: User }> {
    return this.get(`user/${id}`);
  }
}
