import { ApolloServer, CreateHandlerOptions } from "apollo-server-lambda";
import { buildFederatedSchema } from "@apollo/federation";
import UserAPI from "./UserAPI";
import typeDefs from "./schema";
import resolvers from "./resolvers";

const { API_KEY } = process.env;

export const context = ({ event, context }) => ({
  headers: event.headers,
  functionName: context.functionName,
  token: API_KEY,
  event,
  context
});

export const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }]),
  subscriptions: false,
  playground: true,
  dataSources: () => ({ userAPI: new UserAPI() }),
  context
});

const createHandlerOptions: CreateHandlerOptions = {
  cors: {
    origin: "*",
    credentials: true
  }
};

export const serverHandler = server.createHandler(createHandlerOptions);

export default serverHandler;
