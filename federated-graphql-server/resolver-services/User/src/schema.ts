import { gql } from "apollo-server-lambda";

export default gql`
  type Query {
    me(id: ID!): User
  }

  type User @key(fields: "id") {
    id: ID!
    username: String
  }
`;
