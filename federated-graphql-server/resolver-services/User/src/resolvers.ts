import { UserResolvers } from "./types";

export default {
  Query: {
    me: async (_source, { id }, { dataSources }): Promise<UserResolvers> => {
      const { data } = await dataSources.userAPI.getUser(id);

      return data;
    }
  }

  // the buildFederatedSchema function allows a resolver map to define a new
  // special field called __resolveReference for each type that is an entity.
  // This function is called whenever an entity is requested as part of the fufilling a query plan
  // User: {
  //   __resolveReference(user, { fetchUserById }) {
  //     return fetchUserById(user.id);
  //   }
  // }
};
