export {};

import { ApolloServer } from "apollo-server-lambda";
import { buildFederatedSchema } from "@apollo/federation";
import { context as defaultContext } from "../src/handler";
import typeDefs from "../src/schema";
import resolvers from "../src/resolvers";

import UserAPI from "../src/UserAPI";

export const constructTestServer = ({ context = defaultContext } = {}) => {
  const userAPI = new UserAPI();

  const server = new ApolloServer({
    schema: buildFederatedSchema([{ typeDefs, resolvers }]),
    dataSources: () => ({ userAPI }),
    context
  });

  return { server, userAPI };
};
