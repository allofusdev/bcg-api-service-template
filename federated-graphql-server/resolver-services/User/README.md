# User Resolver Service

## Usage

The service is run from the parent directory. It runs a simple lambda function behind an api gateway, which exposes it's methods to the apollo federated server.

```
    Install dependancies

    $ npm install

    Configure the graphql codegen

    $ npx graphql-codegen --config codegen.yml
```

## Type Generation

We can generate types from our schema using the graphql-codegen npm library.

```
    $ npm run generate
```

## Testing

We test our resolver services using jest. Apollo offer a coherent testing strategy with tools to do so.

### Resolver unit testing

We can test our resolvers return the data they should by importing the resolvers object in `handler.ts` and mocking the graphql
context to run the function decoupled from the server.

    Consider the following resolver object.

    export const resolvers = {
        Query: {
            me: async (_source, { id }, { dataSources }): Promise<User> => {
            const { data } = await dataSources.userAPI.getUser(id);

            return data;
            }
        }
    };

    We can access this function directly, by injecting a mock id and returning a mocked context.

    const mockResponse = {
        id: "7a6a2cca-9092-455f-bc24-639f75709a7c",
        username: "@tom"
    };

    const mockContext = {
        dataSources: {
        userAPI: {
            getUser: jest.fn()
        }
        },
        token: ""
    };

    getUser.mockReturnValue({
        data: mockResponse
    });

    await resolvers.Query.me(null, { id: mockResponse.id }, mockContext);

### Integration tests

One option to integration testing is to run queries against a live api, this may be required with more complex api's, but these processes can add time to build. Apollo offers utilities to integration test against a test client, removing the need to query against a test environment in most cases.

    We have a utility to construct a test server, passing a mocked context to the client.

    const { server, userAPI } = constructTestServer({
      context: () => ({
        headers: {},
        functionName: "",
        token: "test-string",
        event: {},
        context: {}
      })
    });

    In this case we simply need to mock our data source outputs (from the user REST api) with jest.

    userAPI.getUser = jest.fn(() =>
      Promise.resolve({
        data: mockResponse
      })
    );

    Then we can simply instanstiate a test client and run queries and mutations.

    const { query } = createTestClient(server);

    query({
      query: QUERY_ME,
      variables: { id: mockResponse.id }
    });

### Data source testing

    Apollo offers utilities for constructing clients for data sources, these can be tested with jest like normal js classes by binding the class getter and setter to the apollo RESTDataSource class.

    For example, a getter...

    const mocks = {
        get: jest.fn()
    };

    ds.get = mocks.get;

    mocks.get.mockReturnValueOnce(Promise.resolve({ data: mockResponse }));
