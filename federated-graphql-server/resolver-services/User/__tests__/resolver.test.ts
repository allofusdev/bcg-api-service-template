export {};

import resolvers from "../src/resolvers";

const mockResponse = {
  id: "7a6a2cca-9092-455f-bc24-639f75709a7c",
  username: "@tom"
};

describe("[Query.me]", () => {
  const mockContext = {
    dataSources: {
      userAPI: {
        getUser: jest.fn()
      }
    },
    token: ""
  };

  const { getUser } = mockContext.dataSources.userAPI;

  getUser.mockReturnValue({
    data: mockResponse
  });

  it("Returns user data", async () => {
    const res = await resolvers.Query.me(
      null,
      { id: mockResponse.id },
      mockContext
    );

    expect(res).toEqual(mockResponse);
  });
});
