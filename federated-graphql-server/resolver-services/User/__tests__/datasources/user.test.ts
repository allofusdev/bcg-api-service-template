export {};

import UserAPI from "../../src/UserAPI";

const mockResponse = {
  id: "7a6a2cca-9092-455f-bc24-639f75709a7c",
  username: "@tom"
};

const { id } = mockResponse;

const mocks = {
  get: jest.fn()
};

const ds = new UserAPI();

// @ts-expect-error
// ^~~~~~~~~~~~~~~^ error: "Property 'get' is protected and only accessible within class 'RESTDataSource<TContext>' and its subclasses.ts(2445)"
ds.get = mocks.get;

ds.initialize({ cache: null, context: { token: "kljdasjfal" } });

describe("[UserApi.getUser]", () => {
  it("looks up user from api by id", async () => {
    mocks.get.mockReturnValueOnce(Promise.resolve({ data: mockResponse }));
    const res = await ds.getUser(id);

    expect(res).toEqual({ data: { ...mockResponse } });
    expect(mocks.get).toBeCalledWith(`user/${id}`);
  });
});
