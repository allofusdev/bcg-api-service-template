import { createTestClient } from "apollo-server-testing";
import gql from "graphql-tag";

import { constructTestServer } from "../testing-utils";

const mockResponse = {
  id: "7a6a2cca-9092-455f-bc24-639f75709a7c",
  username: "@tom"
};

const QUERY_ME = gql`
  query($id: ID!) {
    me(id: $id) {
      id
      username
    }
  }
`;

describe("Query integration tests", () => {
  it("Fetches the me query", async () => {
    const { server, userAPI } = constructTestServer({
      context: () => ({
        headers: {},
        functionName: "",
        token: "test-string",
        event: {},
        context: {}
      })
    });

    userAPI.getUser = jest.fn(() =>
      Promise.resolve({
        data: mockResponse
      })
    );

    const { query } = createTestClient(server);

    const res = await query({
      query: QUERY_ME,
      variables: { id: mockResponse.id }
    });

    expect(res).toMatchSnapshot();
  });
});
