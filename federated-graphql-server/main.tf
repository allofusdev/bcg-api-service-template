# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEVELOPMENT CONFIG
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

provider "aws" {
  region = "us-east-1"
}

module "nginx-node-ec2" {
  source              = "../core-infrastucture/modules/nginx-node-ec2"
  # Pull temporary source code from federated-graphql-server-temp
  docker_compose_path = "docker-compose.yml"
  nginx_config_path   = "./nginx"
  app_source_path     = "./app"
  public_key_path     = var.public_key_path
  private_key_path    = var.private_key_path
  
  providers = {
    aws = aws
  }
}